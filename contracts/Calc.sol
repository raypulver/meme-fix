pragma solidity >=0.5.0;

import "@openzeppelin/contracts/math/SafeMath.sol";
import { FractionalExponents } from "./FractionalExponents.sol";

contract Calc is FractionalExponents {
	using SafeMath for uint256;
  uint256 constant rootN =  135;
  uint256 constant rootD = 100;
  uint32 constant expN = 1;
  uint32 constant expD = 4;
  uint256 constant multiplyer = 10000;

	function earned(uint256 depositEpoch, uint256 currentEpoch, uint256 amount) public view returns (uint256 n, uint256 d, uint256 rewardRate, uint256 earnedAmount) {
    // psuedomath: rewardRate = (1.35 * (amount * 1000)^(1/4)) / 86400
		// uint256 blockTime = currentEpoch; // block.timestamp;
		(uint256 rewardRateMantissa, uint8 rewardRateExponent) = power(multiplyer.mul(amount), uint256(1e18), expN, expD); // get the reward rate expressed in fixed point (amount*multiplyer)^exponent
    uint256 rewardRateNoCoefficient = rewardRateMantissa.mul(uint256(1 ether)).div(uint256(1) << uint256(rewardRateExponent)); // convert to realAmount*1e18
    rewardRate = rewardRateNoCoefficient.mul(rootN).mul(86400).div(rootD); // multiply by (1.35/86400)
		n = amount.mul(multiplyer);
		d = uint256(1 ether).mul(rootN).div(rootD);
    earnedAmount = rewardRate.mul(currentEpoch.sub(depositEpoch));
	}
}
